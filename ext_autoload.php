<?php
$phpexcelLibraryPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('phpexcel_library');

return array(
    'PHPExcel' => $phpexcelLibraryPath . 'phpexcel/Classes/PHPExcel.php',
	'PHPExcel_IOFactory' => $phpexcelLibraryPath . 'phpexcel/Classes/PHPExcel/IOFactory.php',
    'PHPExcel_Writer_Excel5' => $phpexcelLibraryPath . 'phpexcel/Classes/PHPExcel/Writer/Excel5.php', 
    'PHPExcel_Style_NumberFormat' => $phpexcelLibraryPath . 'phpexcel/Classes/PHPExcel/style/NumberFormat.php', 
);
?>
<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TaoJiang.' . $_EXTKEY,
	'Volunteer',
	array(
		'VolunteerUser' => '',
		'Department' => '',
		'Job' => '',
		'ManagerUser' => '',
        'JobEvaluation' => '',
        'Competent' => '',
	),
	// non-cacheable actions
	array(
		'VolunteerUser' => 'auditUpdate,stopWork, export',
		'Department' => 'create, update, delete',
		'Job' => 'create, update, delete',
        'JobEvaluation' => 'create, update, delete, export, certify',
        'Competent' => 'create, update, delete'
	)
);

<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_mfwcvolunteer_domain_model_job'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_mfwcvolunteer_domain_model_job']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, level, type, description, goal, qualifications, competent',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, level, type, description, goal, qualifications, competent, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mfwcvolunteer_domain_model_job',
				'foreign_table_where' => 'AND tx_mfwcvolunteer_domain_model_job.pid=###CURRENT_PID### AND tx_mfwcvolunteer_domain_model_job.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'level' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.level',
			'config' => array(
				'type' => 'radio',
				'items' => array(
					array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.level.I.0', 0),
                    array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.level.I.1', 1),
				),
			),
		),
		'type' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.type',
			'config' => array(
				'type' => 'radio',
				'items' => array(
					array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.type.I.0', 0),
                    array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.type.I.1', 1),
                    array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.type.I.2', 2),
				),
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'goal' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.goal',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'qualifications' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.qualifications',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'competent' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_job.competent',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_mfwcvolunteer_domain_model_competent',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
	),
);

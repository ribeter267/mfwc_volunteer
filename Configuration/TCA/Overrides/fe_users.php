<?php

if (!isset($GLOBALS['TCA']['fe_users']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['fe_users']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumnstx_mfwcjihe_fe_users = array();
	$tempColumnstx_mfwcjihe_fe_users[$GLOBALS['TCA']['fe_users']['ctrl']['type']] = array(
		'exclude' => 1,
		'label'   => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('FrontendUser','Tx_MfwcJihe_FrontendUser')
			),
			'default' => 'Tx_MfwcJihe_FrontendUser',
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumnstx_mfwcjihe_fe_users, 1);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'fe_users',
	$GLOBALS['TCA']['fe_users']['ctrl']['type'],
	'',
	'after:' . $GLOBALS['TCA']['fe_users']['ctrl']['label']
);

/* inherit and extend the show items from the parent class */

if(isset($GLOBALS['TCA']['fe_users']['types']['0']['showitem'])) {
	$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_FrontendUser']['showitem'] = $GLOBALS['TCA']['fe_users']['types']['0']['showitem'];
} elseif(is_array($GLOBALS['TCA']['fe_users']['types'])) {
	// use first entry in types array
	$fe_users_type_definition = reset($GLOBALS['TCA']['fe_users']['types']);
	$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_FrontendUser']['showitem'] = $fe_users_type_definition['showitem'];
} else {
	$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_FrontendUser']['showitem'] = '';
}
$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_FrontendUser']['showitem'] .= ',--div--;LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_frontenduser,';
$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_FrontendUser']['showitem'] .= '';

$GLOBALS['TCA']['fe_users']['columns'][$GLOBALS['TCA']['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_MfwcJihe_FrontendUser','Tx_MfwcJihe_FrontendUser');

$tmp_mfwc_jihe_columns = array(

	'tx_mfwcshoolinfos_id' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_school.tx_mfwcshoolinfos_id',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'tx_mfwcshoolinfos_weibo' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_school.tx_mfwcshoolinfos_weibo',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'tx_mfwcshoolinfos_area' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_school.tx_mfwcshoolinfos_area',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_mfwcshoolinfos_area',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
	'jihe_group' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_school.jihe_group',
		'config' => array(
			'type' => 'select',
			'foreign_table' => 'tx_mfwcjihe_domain_model_group',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_mfwc_jihe_columns);

/* inherit and extend the show items from the parent class */

if(isset($GLOBALS['TCA']['fe_users']['types']['0']['showitem'])) {
	$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_School']['showitem'] = $GLOBALS['TCA']['fe_users']['types']['0']['showitem'];
} elseif(is_array($GLOBALS['TCA']['fe_users']['types'])) {
	// use first entry in types array
	$fe_users_type_definition = reset($GLOBALS['TCA']['fe_users']['types']);
	$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_School']['showitem'] = $fe_users_type_definition['showitem'];
} else {
	$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_School']['showitem'] = '';
}
$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_School']['showitem'] .= ',--div--;LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_school,';
$GLOBALS['TCA']['fe_users']['types']['Tx_MfwcJihe_School']['showitem'] .= 'tx_mfwcshoolinfos_id, tx_mfwcshoolinfos_weibo, tx_mfwcshoolinfos_area, jihe_group';

$GLOBALS['TCA']['fe_users']['columns'][$GLOBALS['TCA']['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_MfwcJihe_School','Tx_MfwcJihe_School');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'',
	'EXT:/Resources/Private/Language/locallang_csh_.xlf'
);
<?php

if (!isset($GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumnstx_mfwcjihe_tx_mfwcshoolinfos_area = array();
	$tempColumnstx_mfwcjihe_tx_mfwcshoolinfos_area[$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['type']] = array(
		'exclude' => 1,
		'label'   => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('Area','Tx_MfwcJihe_Area')
			),
			'default' => 'Tx_MfwcJihe_Area',
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mfwcshoolinfos_area', $tempColumnstx_mfwcjihe_tx_mfwcshoolinfos_area, 1);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'tx_mfwcshoolinfos_area',
	$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['type'],
	'',
	'after:' . $GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['label']
);

$tmp_mfwc_jihe_columns = array(

	'title' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_area.title',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_mfwcshoolinfos_area',$tmp_mfwc_jihe_columns);

/* inherit and extend the show items from the parent class */

if(isset($GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['0']['showitem'])) {
	$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['Tx_MfwcJihe_Area']['showitem'] = $GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['0']['showitem'];
} elseif(is_array($GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types'])) {
	// use first entry in types array
	$tx_mfwcshoolinfos_area_type_definition = reset($GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']);
	$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['Tx_MfwcJihe_Area']['showitem'] = $tx_mfwcshoolinfos_area_type_definition['showitem'];
} else {
	$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['Tx_MfwcJihe_Area']['showitem'] = '';
}
$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['Tx_MfwcJihe_Area']['showitem'] .= ',--div--;LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcjihe_domain_model_area,';
$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['types']['Tx_MfwcJihe_Area']['showitem'] .= 'title';

$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['columns'][$GLOBALS['TCA']['tx_mfwcshoolinfos_area']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:mfwc_jihe/Resources/Private/Language/locallang_db.xlf:tx_mfwcshoolinfos_area.tx_extbase_type.Tx_MfwcJihe_Area','Tx_MfwcJihe_Area');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'',
	'EXT:/Resources/Private/Language/locallang_csh_.xlf'
);
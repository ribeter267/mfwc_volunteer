<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_mfwcvolunteer_domain_model_jobevaluation'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_mfwcvolunteer_domain_model_jobevaluation']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, department, job, jobkeep, evluation, description, fromtime, totime, length, recorder',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, department, job, jobkeep, evluation, description, fromtime, totime, length, recorder, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_mfwcvolunteer_domain_model_jobevaluation',
				'foreign_table_where' => 'AND tx_mfwcvolunteer_domain_model_jobevaluation.pid=###CURRENT_PID### AND tx_mfwcvolunteer_domain_model_jobevaluation.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
        
        'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		
		
		'department' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.department',		
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_mfwcvolunteer_domain_model_department',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
		'job' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.job',		
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_mfwcvolunteer_domain_model_job',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
		'jobkeep' => array(		
			'exclude' => 0,		
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.jobkeep',		
			'config' => array(
				'type' => 'input',
			),
		),

        'length' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.length',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'double2'
			),
		),

		'evluation' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.evluation',
			'config' => array(
				'type' => 'radio',
				'items' => array(
					array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.evluation.I.0', 0),
                    array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.evluation.I.1', 1),
                    array('LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.evluation.I.2', 2),
				),
			),
		),
		'description' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.description',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'fromtime' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.fromtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
			),
		),
		'totime' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.totime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
			),
		),
        
        'recorder' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:mfwc_volunteer/Resources/Private/Language/locallang_db.xlf:tx_mfwcvolunteer_domain_model_jobevaluation.recorder',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'fe_users',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
		'volunteeruser' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);

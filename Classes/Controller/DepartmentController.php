<?php
namespace TaoJiang\MfwcVolunteer\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * DepartmentController
 */
class DepartmentController extends \TaoJiang\MfwcVolunteer\Controller\CommonController {

    
    protected function initializeAction(){
    
        if($this->request->hasArgument('department')) {
        
            $department = $this->request->getArgument('department');
            $propertyMappingConfiguration = $this->arguments['department']->getPropertyMappingConfiguration();

            /*** 部门管理员属性  
                **/
            if(isset($department['manager']) && is_array($department['manager'])){
            
                $propertyMappingConfiguration->forProperty('manager')->allowAllProperties();
                foreach($department['manager'] as $Index => $PropertyArray) {
                    $propertyMappingConfiguration->allowCreationForSubProperty('manager.'. $Index);
                    $propertyMappingConfiguration->forProperty('manager.'. $Index)->allowAllProperties();
                }
            }
        }
    }

	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction() {
		$departments = $this->departmentRepository->findAll();
		$this->view->assign('departments', $departments);
	}

	

	/**
	 * action new
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @ignorevalidation $department
	 * @return void
	 */
	public function newAction(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department = NULL) {
        
        if($department === NULL) $department = new \TaoJiang\MfwcVolunteer\Domain\Model\Department();
  

        $this->view->assign('managerUsers', $this->managerUserRepository->findDepartmentUsers($this->settings['departmentGroups']));
		$this->view->assign('department', $department);
	}

	/**
	 * action create
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @return void
	 */
	public function createAction(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
		$this->addFlashMessage('新部门创建成功');
		$this->departmentRepository->add($department);
		$this->redirect('list');
	}

	/**
	 * action edit
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @ignorevalidation $department
	 * @return void
	 */
	public function editAction(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
    
        $this->view->assign('managerUsers', $this->managerUserRepository->findDepartmentUsers($this->settings['departmentGroups']));
		$this->view->assign('department', $department);
	}

	/**
	 * action update
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @return void
	 */
	public function updateAction(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
        
        $this->volunteerUserRepository->updateKeepDepartment($department);
        
		$this->addFlashMessage('更新成功');
		$this->departmentRepository->update($department);
		$this->redirect('list');
	}

	/**
	 * action delete
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @return void
	 */
	public function deleteAction(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
		$this->addFlashMessage('删除成功');
		$this->departmentRepository->remove($department);
		$this->redirect('list');
	}

}
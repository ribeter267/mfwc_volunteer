<?php
namespace TaoJiang\MfwcVolunteer\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * JobEvaluationController
 */
class JobEvaluationController extends \TaoJiang\MfwcVolunteer\Controller\CommonController {
    
     /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;
    
 
    /**
     * INIT
     */
    public function initializeAction (){
        
        if($this->request->hasArgument('jobEvaluation')) {
            $propertyMappingConfiguration = $this->arguments->getArgument('jobEvaluation')->getPropertyMappingConfiguration();
            //时间类型修改
            $propertyMappingConfiguration->forProperty('fromtime')->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d H:i');
            $propertyMappingConfiguration->forProperty('totime')->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\DateTimeConverter::CONFIGURATION_DATE_FORMAT, 'Y-m-d H:i');
        }
    }

    
	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction() {
    
        $keyword = '';
        if($this->request->hasArgument('keyword')) {
            $keyword = $this->request->getArgument('keyword');
        }
        
		$volunteerUsers = $this->volunteerUserRepository->findAllEvaluation($this->settings['volunteerGroups'],$keyword);
		$this->view->assign('volunteerUsers', $volunteerUsers);
        $this->view->assign('keyword', $keyword);
	}
 
 
 
	/**
	 * action show
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @return void
	 */
	public function showAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser) {
    
		/**
        $count = array(
            'y' => 0,
            'l' => 0,
            'j' => 0
        );  $countLength=0;
        
        foreach($volunteerUser->getTxMfwcvolunteerEvaluation() as $evaluation){
            switch($evaluation->getEvluation()){
                case 0:
                    $count['y']++;
                    break;
                case 1:
                    $count['l']++;
                    break;
                case 2:
                    $count['j']++;
            }
            $countLength += $evaluation->getLength();
        }

        if($count['y'] >= $count['l'] && $count['y'] >= $count['j']){
            $evaluationStatus = 'y';
        }else if($count['j'] >= $count['y'] && $count['j'] >= $count['l']){
            $evaluationStatus = 'j';
        }else{
            $evaluationStatus = 'l';
        }
        
        $this->view->assign('countLength', $countLength);
        $this->view->assign('evaluationStatus', $evaluationStatus);
		**/
		$this->view->assign('volunteerUser', $volunteerUser);
		$this->view->assign('userId', $GLOBALS['TSFE']->fe_user->user['uid']);
		$this->view->assign('department', $this->departmentRepository->findDepartmentByUserId($GLOBALS['TSFE']->fe_user->user['uid']));
	}
    
    
    /**
     * 添加 评价
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
     *
     * @return void
     */
    public function newAction(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $jobEvaluation = NULL){
        
        //$volunteerUsers = $this->volunteerUserRepository->findDepartmentVolunteers($this->settings['volunteerGroups'],
		//$this->departmentRepository->findDepartmentByUserId($GLOBALS['TSFE']->fe_user->user['uid']));
		
		$volunteerUsers = $this->volunteerUserRepository->findWorkingVolunteers($this->settings['volunteerGroups']);
		$this->view->assign('volunteerUsers', $volunteerUsers);
        $this->view->assign('jobEvaluation', $jobEvaluation);
		$this->view->assign('jobs', $this->resetSelectJobs());
    }
	
    
    /**
     * 添加 评价
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
     *
     * @return void
     */
    public function innewAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
        
		$this->view->assign('jobs', $this->resetSelectJobs());
        $this->view->assign('volunteerUser', $volunteerUser);
    }
    
	
	
	/**
	 * 重构JOBS， 方便SELECT显示
	 * @return array
	 */
	protected function resetSelectJobs(){
		$jobs = array();
		$temp_departments = $this->departmentRepository->findAll();
		foreach($temp_departments as $temp_department){
			$temp_jobs = $temp_department->getJob();
			foreach($temp_jobs as $temp_job){
				//$job = array(
				//	'uid' => $temp_job->getUid(),
				//	'title' => $temp_department->getTitle() .' - '. $temp_job->getTitle(),
				//);
				$jobs[$temp_job->getUid()] = $temp_department->getTitle() .' - '. $temp_job->getTitle();
			}
		}
		return $jobs;
	}
	
	
    /**
     * 创建保存
     */
    public function createAction(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $jobEvaluation){
        
        
		/**
        if($this->volunteerUserRepository->findExsitEvaluation(
            $this->request->getArgument('volunteer'),
            $jobEvaluation->getFromtime(),
            $jobEvaluation->getTotime()
        )){
            
            $this->addFlashMessage('选择的时间内已有记录');
            $this->redirect('new');
        }else{
		
		*/
        
            $this->addFlashMessage('添加成功');
			$jobEvaluation->setDepartment($this->departmentRepository->findDepartmentByJob($jobEvaluation->getJob()));
			$jobEvaluation->setJobkeep($jobEvaluation->getDepartment()->getTitle() .' - '.$jobEvaluation->getJob()->getTitle());
			
            $jobEvaluation->setRecorder($this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']));
            if($jobEvaluation->getLength() == 0){
                $jobEvaluation->setLength(sprintf("%.2f", ($jobEvaluation->getTotime()->getTimestamp() - $jobEvaluation->getFromtime()->getTimestamp()) / 3600));
            }

            $volunteer = $this->volunteerUserRepository->findByUid($this->request->getArgument('volunteer'));
           
			//$jobEvaluation->setDepartment($volunteer->getTxMfwcvolunteerWorkdepartment());
			//$jobEvaluation->setJob($volunteer->getTxMfwcvolunteerWorkjobs());
			
            if(!$volunteer->getTxMfwcvolunteerAuthenticationTime() || $volunteer->getTxMfwcvolunteerAuthenticationTime() > $jobEvaluation->getFromtime()){
                $volunteer->setTxMfwcvolunteerAuthenticationTime($jobEvaluation->getFromtime());
            }
            $volunteer->addTxMfwcvolunteerEvaluation($jobEvaluation);
            $this->volunteerUserRepository->update($volunteer);
			
			$this->setEvaluationLengthAndStatus($volunteer);

            if($this->request->hasArgument('innew')){
                $this->redirect('show',NULL,NULL,array('volunteerUser' => $volunteer->getUid()));
            }
            
            $this->redirect('list');
        // }
    }
    
    
    /**
     * 编辑
     */
    public function editAction(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $jobEvaluation,\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
        $this->view->assign('volunteerUser', $volunteerUser);
        $this->view->assign('jobEvaluation', $jobEvaluation);
		$this->view->assign('jobs', $this->resetSelectJobs());
    }

    
    
    /**
     * 保存数据
     */
    public function updateAction(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $jobEvaluation){
       
		/**
        if($this->volunteerUserRepository->findExsitEvaluation(
            $this->request->getArgument('volunteer'),
            $jobEvaluation->getFromtime(),
            $jobEvaluation->getTotime(),
            $jobEvaluation->getUid()
        )){
            $this->addFlashMessage('选择的时间内已有记录');
            $this->redirect('new');
        }else{
        
		*/
            $this->addFlashMessage('修改成功');
            $jobEvaluation->setRecorder($this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']));
            $jobEvaluation->setJobkeep($jobEvaluation->getDepartment()->getTitle() .' - '.$jobEvaluation->getJob()->getTitle());
            
            if($jobEvaluation->getLength() == 0){
                $jobEvaluation->setLength(sprintf("%.2f", ($jobEvaluation->getTotime()->getTimestamp() - $jobEvaluation->getFromtime()->getTimestamp()) / 3600));
            }
            
            $volunteer = $this->volunteerUserRepository->findByUid($this->request->getArgument('volunteer'));
            if(!$volunteer->getTxMfwcvolunteerAuthenticationTime() || $volunteer->getTxMfwcvolunteerAuthenticationTime() > $jobEvaluation->getFromtime()){
                $volunteer->setTxMfwcvolunteerAuthenticationTime($jobEvaluation->getFromtime());
            }
   
            $this->volunteerUserRepository->update($volunteer);
            $this->jobEvaluationRepository->update($jobEvaluation);
			
			$this->setEvaluationLengthAndStatus($volunteer);
        
            $this->redirect('show',NULL,NULL,array('volunteerUser' => $volunteer->getUid()));
        // }
    }
    
    
    /**
     * 重新计算 工作时长
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteer
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $jobEvaluation
	 * @param boolean $isDelete
     * @return void
     */
    protected function setEvaluationLengthAndStatus(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteer){
       
		$count = array(
            0 => 0,
            1 => 0,
            2 => 0,
        );  $length = 0; 
		
        if($volunteer->getTxMfwcvolunteerEvaluation() != NULL){
            foreach($volunteer->getTxMfwcvolunteerEvaluation() as $evaluation){
                //if($jobEvaluation == null || !($isDelete && $jobEvaluation->getUid() == $evaluation->getUid())){
                    $length += $evaluation->getLength();
					$count[$evaluation->getEvluation()]++;
               // }
            }
        }
		
		//总工作时间
        $volunteer->setTxMfwcvolunteerEvaluationLength($length);
		
		//总评价状态
		if($count[0] >= $count[1] && $count[0] >= $count[2]){
			$volunteer->setTxMfwcvolunteerEvaluationStatus(0);
        }else if($count[1] >= $count[0] && $count[1] >= $count[2]){
            $volunteer->setTxMfwcvolunteerEvaluationStatus(1);
        }else{
			$volunteer->setTxMfwcvolunteerEvaluationStatus(2);
        }
		$this->volunteerUserRepository->update($volunteer);
		//return $volunteer;
    }
	
	
	
    
    /**
     * 删除
     */
    public function deleteAction(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $jobEvaluation, \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
        
        $this->addFlashMessage('删除成功');
    
        $volunteerUser->removeTxMfwcvolunteerEvaluation($jobEvaluation);
        $this->volunteerUserRepository->update($volunteerUser);
        $this->jobEvaluationRepository->remove($jobEvaluation);
		
		$this->setEvaluationLengthAndStatus($volunteerUser);
   
        $this->redirect('show',NULL,NULL,array('volunteerUser' => $volunteerUser->getUid()));
    }
	
	/**
	 * 导出记录
	 * @return void
	 */
	public function exportAction(){
		
		$volunteers = $this->volunteerUserRepository->findAllEvaluation($this->settings['volunteerGroups']);
		
		$objPHPExcel = $this->objectManager->create('PHPExcel');
        $sheet  = $objPHPExcel->setActiveSheetIndex(0); 
        $sheet->setCellValue('A1', '编号');
        $sheet->setCellValue('B1', '姓名');
        $sheet->setCellValue('C1', '工作所属岗位');
        $sheet->setCellValue('D1', '开始时间');
        $sheet->setCellValue('E1', '结束时间');
        $sheet->setCellValue('F1', '工作内容');
        $sheet->setCellValue('G1', '工作时长(小时)');
        $sheet->setCellValue('H1', '工作评价');
        $sheet->setCellValue('I1', '记录人');
		
		$sheet->getColumnDimension('A')->setWidth(20); 
		$sheet->getColumnDimension('B')->setWidth(10);
		$sheet->getColumnDimension('C')->setWidth(18);
		$sheet->getColumnDimension('D')->setWidth(23);
		$sheet->getColumnDimension('E')->setWidth(23);
		$sheet->getColumnDimension('F')->setWidth(20);
		$sheet->getColumnDimension('G')->setWidth(10);
		$sheet->getColumnDimension('H')->setWidth(10);
		$sheet->getColumnDimension('I')->setWidth(10);
		
        
        $index_i = 2;
		if($volunteers->count() > 0){
			foreach($volunteers as $volunteer){
				if(count($volunteer->getTxMfwcvolunteerEvaluation()) > 0){
					foreach($volunteer->getTxMfwcvolunteerEvaluation() as $evaluation){
			
						$status = '优秀'; 
						if($evaluation->getEvluation() == 1) $status = '良好';
						if($evaluation->getEvluation() == 2) $status = '合格';
					
						$sheet->setCellValue('A'.$index_i, ' '.$volunteer->getTxMfwcvolunteerId());
						$sheet->setCellValue('B'.$index_i, $volunteer->getName());
						$sheet->setCellValue('C'.$index_i, ($evaluation->getJobkeep()));
						$sheet->setCellValue('D'.$index_i, $evaluation->getFromtime()->format('Y年m月d日 H:i'));
						$sheet->setCellValue('E'.$index_i, $evaluation->getTotime()->format('Y年m月d日  H:i'));
						$sheet->setCellValue('F'.$index_i, $evaluation->getDescription());
						$sheet->setCellValue('G'.$index_i, $evaluation->getLength());
						$sheet->setCellValue('H'.$index_i, $status);
						$sheet->setCellValue('I'.$index_i, $evaluation->getRecorder()->getName());
						$index_i++;
						//break;
					}
				}
			}
		}
        
        $objWriter = $this->objectManager->create('PHPExcel_Writer_Excel5',$objPHPExcel);
		$fileName = iconv('UTF-8', 'GB2312', '志愿者记录评价表');
		header('Pragma: public');  
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');  
		header('Content-Type: application/force-download');  
		header('Content-Type: application/vnd.ms-excel');  
		header('Content-Disposition: attachment;filename="' . $fileName . '.xls"');  
		header('Cache-Control: max-age=0');  
		$objWriter->save('php://output'); 
		exit;
	}
    
    
    /**
     * 证书生成
     *
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
     * @return void
     */
    public function certifyAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){

        $job1 = ''; $job2 = ''; $i=0;
        $jobs = array();
        foreach($volunteerUser->getTxMfwcvolunteerEvaluation() as  $evaluation){
            $tempJob = explode('-',$evaluation->getJobkeep());
            if(isset($tempJob[1]) && !in_array($tempJob[1], $jobs)){
                $jobs[] = $tempJob[1];
            }
        }
        
        
        if(count($jobs) > 0){ //服务多于一个职位时
        
            foreach($jobs as $job){
                if($i == 0){
                    $job1 = $job;
                }else{
                    $job2 .= $job;
                    if(count($jobs) > 2) $job2 .= ' 等';
                    break;
                }
                $i++;
            }
            
            
            $matiralPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('mfwc_volunteer') . 'Resources/Public/Images/certify/';
            $templateFile = $matiralPath . 'certify_bg.gif';
            $fontFile = $matiralPath . 'msyh.ttf';
        
            $name = $volunteerUser->getName();
            $day = $volunteerUser->getTxMfwcvolunteerAuthenticationTime()->format('Y年m月'); 
            $dayEn = $volunteerUser->getTxMfwcvolunteerAuthenticationTime()->format('Y-m');
            $time = $volunteerUser->getTxMfwcvolunteerEvaluationLength();
            $number = strtoupper('MFWC-V-'.$volunteerUser->getTxMfwcvolunteerId());
            
            $newFile = PATH_site.'uploads/tx_mfwcvolunteer/'.$number.'.gif';
            $param = "-resize 638x946 -font {$fontFile} -fill black -pointsize 14 -colorspace RGB -quality 80";
            $param .= " -draw 'text 105,360 \"{$name}\"";
            $param .= " text 172,395 \"{$day}\"";
            $param .= " text 435,395 \"{$job1}\"";
            $param .= " text 100,430 \"{$job2}\"";
            $param .= " text 180,470 \"{$time}\"";
            $param .= " text 245,600 \"{$name}\"";
            $param .= " text 395,640 \"{$dayEn}\"";
            $param .= " text 125,675 \"{$time}\"";
            $param .= " text 360,875 \"{$number}\"'";
            
            $gifCreator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('\TYPO3\CMS\Core\Imaging\GraphicalFunctions');
            $gifCreator->init();
            $gifCreator->imageMagickExec($templateFile, $newFile, $param);

            header('Content-type: application/x-gif');
            header('Content-Disposition: attachment; filename='.$number.'.gif');
            header('Content-Length: '.filesize($newFile));
            readfile($newFile);
            exit;
        }
        
        
        $this->addFlashMessage('证书生成失败');
        $this->redirect('show',NULL,NULL,array('volunteerUser' => $volunteerUser->getUid()));
    }
	
}
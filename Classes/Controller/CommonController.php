<?php
namespace TaoJiang\MfwcVolunteer\Controller;

/**
 * CommonController
 */
class CommonController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\CompetentRepository
	 * @inject
	 */
	protected $competentRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\DepartmentManagerRepository
	 * @inject
	 */
	protected $departmentManagerRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\DepartmentRepository
	 * @inject
	 */
	protected $departmentRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\EvaluationRepository
	 * @inject
	 */
	protected $evaluationRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\JobEvaluationRepository
	 * @inject
	 */
	protected $jobEvaluationRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\JobRepository
	 * @inject
	 */
	protected $jobRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\ManagerUserRepository
	 * @inject
	 */
	protected $managerUserRepository;
    
     /**
	 * @var \TaoJiang\MfwcVolunteer\Domain\Repository\VolunteerUserRepository
	 * @inject
	 */
	protected $volunteerUserRepository;
    
   
    
    /**
     * 发送邮件
     * @param array $receiver
     * @param string $subject
     * @param string $body
     * //$mail->setTo(array($email => $name));
     * //$mail->setFrom(array($email => $name));
     * return void
     */
    protected function sendmail($receiver,$subject, $body){

        $mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Mail\\MailMessage');
        $mail->setFrom(\TYPO3\CMS\Core\Utility\MailUtility::getSystemFrom());
        $mail->setTo($receiver);
        $mail->setSubject($subject);
        $mail->setBody($body,'text/html');
        $mail->send();
    }
    
}
<?php
namespace TaoJiang\MfwcVolunteer\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * JobController
 */
class JobController extends \TaoJiang\MfwcVolunteer\Controller\CommonController {

	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction() {
		$jobs = $this->jobRepository->findAll();
		$this->view->assign('jobs', $jobs);
	}

	

	/**
	 * action new
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @ignorevalidation $job
	 * @return void
	 */
	public function newAction(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job = NULL, \TaoJiang\MfwcVolunteer\Domain\Model\Department $department = NULL) {
    
		$this->view->assign('job', $job);
        $this->view->assign('competents', $this->competentRepository->findAll());
        $this->view->assign('department', $department);
        $this->view->assign('departments', $this->departmentRepository->findAll());
	}

	/**
	 * action create
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @return void
	 */
	public function createAction(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job) {

        if($this->request->hasArgument('department')) {
            
            $department = $this->departmentRepository->findByUid($this->request->getArgument('department'));
            $department->addJob($job);
            $this->departmentRepository->update($department);
            
            $this->addFlashMessage('岗位添加成功');
            $this->redirect('edit', 'Department',NULL,array('department' => $department->getUid()));
        }else{
        
            $this->addFlashMessage('没有指定岗位信息');
            $this->redirect('new');
        }
        
		
	}

	/**
	 * action edit
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @ignorevalidation $job
	 * @return void
	 */
	public function editAction(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job, \TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
		
        $this->view->assign('job', $job);
        $this->view->assign('competents', $this->competentRepository->findAll());
        $this->view->assign('department', $department);
	}

	/**
	 * action update
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @return void
	 */
	public function updateAction(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job, \TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
		
        //更新KEEP的部门
        $this->volunteerUserRepository->updateKeepJob($department, $job);
        
        $this->addFlashMessage('更新成功');
		$this->jobRepository->update($job);
        $this->redirect('edit', 'Department',NULL,array('department' => $department->getUid()));
	}

	/**
	 * action delete
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @return void
	 */
	public function deleteAction(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job, \TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
		$this->addFlashMessage('岗位删除成功');
		//$this->jobRepository->remove($job);
        
        $department->removeJob($job);
        $this->departmentRepository->update($department);
		$this->redirect('edit', 'Department',NULL,array('department' => $department->getUid()));
	}

}
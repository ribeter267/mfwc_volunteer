<?php
namespace TaoJiang\MfwcVolunteer\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * VolunteerUserController
 */
class VolunteerUserController extends \TaoJiang\MfwcVolunteer\Controller\CommonController {

	/**
	 * 志愿者列表
	 * 
	 * @return void
	 */
	public function listAction() {
        
        $keyword = $this->request->hasArgument('keyword') ?  $this->request->getArgument('keyword') : '';
		$volunteerUsers = $this->volunteerUserRepository->findAllVolunteers($this->settings['volunteerGroups'],$keyword);
		//$mangerUser = $this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        
		$this->view->assign('volunteerUsers', $volunteerUsers);
		//$this->view->assign('mangerUser', $mangerUser);
        $this->view->assign('keyword', $keyword);
        $this->view->assign('pageUid', $GLOBALS['TSFE']->id);
        $this->view->assign('returnUrl',\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
	}

	/**
	 * action show
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @return void
	 */
	public function showAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser) {
		$this->view->assign('volunteerUser', $volunteerUser);
	}

    
    /**
	 * 审批
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @ignorevalidation $volunteerUser
	 * @return void
	 */
	public function auditAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser) {

		$this->view->assign('volunteerUser', $volunteerUser);
		$this->view->assign('department', $this->departmentRepository->findDepartmentByUserId($GLOBALS['TSFE']->fe_user->user['uid']));
	}
    
    /**
	 * 审批更新
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @return void
	 */
	public function auditUpdateAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser) {
		$this->addFlashMessage('志愿者认证状态更新成功');
		
		$volunteerUser->setTxMfwcvolunteerStatus(1); //认证通过
		$volunteerUser->setTxMfwcvolunteerWorkdepartment($this->departmentRepository->findDepartmentByUserId($GLOBALS['TSFE']->fe_user->user['uid']));
		$volunteerUser->setTxMfwcvolunteerVolunteermanager($this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']));
		$volunteerUser->setTxMfwcvolunteerWorkjobskeep($volunteerUser->getTxMfwcvolunteerWorkdepartment()->getTitle() .' - '.$volunteerUser->getTxMfwcvolunteerWorkjobs()->getTitle());
		
		
		$this->volunteerUserRepository->update($volunteerUser);
		
		
        if($this->request->hasArgument('sendmail') && $this->request->getArgument('sendmail')) {
            $this->mailMaker($volunteerUser); //发送邮件
        }
        //$this->redirect('audit', NULL, NULL, array('volunteerUser' => $volunteerUser->getUid()));
		
		
		if($this->request->hasArgument('returnAction')) {
			$this->redirect($this->request->getArgument('returnAction'));
		}
	    $this->redirect('list');
	}
    
    /**
     * 生成邮件
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @return void
	 */
    protected function mailMaker(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteer){
        
        $mangerUser = $this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        
        $subject = '免费午餐志愿者审批通知';
        $body = '
            <html>
                <head></head>
                <body>
                    <p>您好:'.$volunteer->getName().'</p><br/>
                    <p>您的志愿者申请已经得到确认和认证，岗位如下:</p><br/>
                    <p></p>
                    <p>'.$volunteer->getTxMfwcvolunteerWorkdepartment()->getTitle().' '.$volunteer->getTxMfwcvolunteerWorkjobs()->getTitle().'</p><br/>
                    <p></p>
                    <p>工作事宜请与 '.$mangerUser->getName().'联系，联系电话'.$mangerUser->getTelephone().'</p><br/>
                    <p>祝好</p>
                </body>
            </html>
        ';

        $recevier = array($volunteer->getEmail() => $volunteer->getName());
        $this->sendmail($recevier,$subject,$body);
    }
    
	
	
	/**
	 * 显示本部门所有志愿者
	 * @return void
	 */
	public function dVListAction(){
		
		$keyword = $this->request->hasArgument('keyword') ?  $this->request->getArgument('keyword') : '';
		$department = $this->departmentRepository->findDepartmentByUserId($GLOBALS['TSFE']->fe_user->user['uid']);
		
		$volunteerUsers = $this->volunteerUserRepository->findDepartmentVolunteers($this->settings['volunteerGroups'],
			$department,
			$keyword);

		$this->view->assign('volunteerUsers', $volunteerUsers);
        $this->view->assign('keyword', $keyword);
        $this->view->assign('pageUid', $GLOBALS['TSFE']->id);
		$this->view->assign('department', $department);
        $this->view->assign('returnUrl',\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
	}

	/**
	 * 查看志愿者信息
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @return void
	 */ 
	public function dVShowAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
		
		$this->view->assign('manager',$this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']));
		$this->view->assign('department', $this->departmentRepository->findDepartmentByUserId($GLOBALS['TSFE']->fe_user->user['uid']));
		$this->view->assign('volunteerUser', $volunteerUser);
	}
	
	
	/**
	 * 暂停志愿者工作
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
	 * @return void
	 */
	public function stopWorkAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser) {
		$this->addFlashMessage('志愿者信息更新成功');
		$volunteerUser->setTxMfwcvolunteerStatus(2);
		$volunteerUser->resetTxMfwcvolunteerWorkdepartment();
		$volunteerUser->resetTxMfwcvolunteerWorkjobs();
		$volunteerUser->resetTxMfwcvolunteerWorkjobskeep();
		$volunteerUser->resetTxMfwcvolunteerVolunteermanager();
		$this->volunteerUserRepository->update($volunteerUser);
		
		if($this->request->hasArgument('returnUrl')) $this->redirectToUri($this->request->getArgument('returnUrl'));
        else $this->redirect('dVList');
	}
	
	
	/**
	 * 工作记录查看
	 */
	public function evaluationShowAction(){
		
		$volunteerUser = $this->volunteerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        $this->view->assign('userId', $GLOBALS['TSFE']->fe_user->user['uid']);
		$this->view->assign('volunteerUser', $volunteerUser);
	}
    
	
	/**
     * 所有志愿者导出
     */
    public function exportAction(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department = NULL){
	
		if($department == NULL) $volunteerUsers = $this->volunteerUserRepository->findAllVolunteers($this->settings['volunteerGroups']);
		else $volunteerUsers = $this->volunteerUserRepository->findDepartmentVolunteers($this->settings['volunteerGroups'],$department);
		
        
        //echo $volunteerUsers->count();
        //exit;
        
		$this->doExport($volunteerUsers);
	}
    
    /**
     * 导出数据
     */
    protected function doExport($volunteerUsers){
	
        $objPHPExcel = $this->objectManager->create('PHPExcel');
        $sheet  = $objPHPExcel->setActiveSheetIndex(0); 
        $sheet->setCellValue('A1', '编号');
        $sheet->setCellValue('B1', '用户名');
        $sheet->setCellValue('C1', '姓名');
        $sheet->setCellValue('D1', '性别');
        $sheet->setCellValue('E1', '所在省份');
        $sheet->setCellValue('F1', '所在市');
        $sheet->setCellValue('G1', '审核状态');
        $sheet->setCellValue('H1', '邮箱');
        $sheet->setCellValue('I1', '手机号码');
        $sheet->setCellValue('J1', 'QQ');
        $sheet->setCellValue('K1', '微博地址');
        $sheet->setCellValue('L1', '申请岗位');
		$sheet->setCellValue('M1', '任职岗位');
        $sheet->setCellValue('N1', '特长');
        $sheet->setCellValue('O1', '是否有志愿者经验');
        $sheet->setCellValue('P1', '工作职责说明');
        
        $index_i = 2;
		if($volunteerUsers->count() > 0){
			foreach($volunteerUsers as $volunteer){
			
				//debug($volunteer->getZone(),'zone');
			   // exit;
			   
				$status = '申请中'; 
				if($volunteer->getTxMfwcvolunteerStatus() == 1) $status = '工作中';
				if($volunteer->getTxMfwcvolunteerStatus() == 2) $status = '暂停工作';
			
				$sheet->setCellValue('A'.$index_i, ' '.$volunteer->getTxMfwcvolunteerId());
				$sheet->setCellValue('B'.$index_i, ' '.$volunteer->getUsername());
				$sheet->setCellValue('C'.$index_i, $volunteer->getName());
				$sheet->setCellValue('D'.$index_i, $volunteer->getGender() ? '女' : '男');
				$sheet->setCellValue('E'.$index_i, $volunteer->getZone() ? $volunteer->getZone()->getTitle() : '');
				$sheet->setCellValue('F'.$index_i, $volunteer->getCity() ? $volunteer->getCity()->getTitle() : '');
				$sheet->setCellValue('G'.$index_i, $status);
				$sheet->setCellValue('H'.$index_i, $volunteer->getEmail());
				$sheet->setCellValue('I'.$index_i, ' '.$volunteer->getTelephone());
				$sheet->setCellValue('J'.$index_i, ' '.$volunteer->getTxMfwcshoolinfosQq());
				$sheet->setCellValue('K'.$index_i, $volunteer->getTxMfwcshoolinfosWeibo());
				$sheet->setCellValue('L'.$index_i, $volunteer->getTxMfwcvolunteerJobskeep());
				$sheet->setCellValue('M'.$index_i, $volunteer->getTxMfwcvolunteerWorkjobskeep());
				$sheet->setCellValue('N'.$index_i, $volunteer->getTxMfwcvolunteerSkills());
				$sheet->setCellValue('O'.$index_i, $volunteer->getTxMfwcvolunteerExperience() ? '是' : '否');
				$sheet->setCellValue('P'.$index_i, $volunteer->getTxMfwcvolunteerResponsibilities());
				$index_i++;
				//break;
			}
		}
        
        $objWriter = $this->objectManager->create('PHPExcel_Writer_Excel5',$objPHPExcel);
        $objWriter->setPreCalculateFormulas(false); 

		$fileName = iconv('UTF-8', 'GB2312', '志愿者信息');
		header('Pragma: public');  
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');  
		header('Content-Type: application/force-download');  
		header('Content-Type: application/vnd.ms-excel');  
		header('Content-Disposition: attachment;filename="' . $fileName . '.xls"');  
		header('Cache-Control: max-age=0');  
     
        
        //$path = 'uploads/tx_mfwcshoolinfos/';
        //$name = date('Y-m-d H:i:s').'.xls';
        //$objWriter->save($path . $name); 
        //echo $path . $name;
		$objWriter->save('php://output'); 
		exit;
    }

}
<?php
namespace TaoJiang\MfwcJihe\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * JiheController
 */
class JiheController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * jiheRepository
	 * 
	 * @var \TaoJiang\MfwcJihe\Domain\Repository\JiheRepository
	 * @inject
	 */
	protected $jiheRepository = NULL;

	/**
	 * action list
	 * 
	 * @return void
	 */
	public function listAction() {
		$jihes = $this->jiheRepository->findAll();
		$this->view->assign('jihes', $jihes);
	}

	/**
	 * action new
	 * 
	 * @return void
	 */
	public function newAction() {
		
	}

	/**
	 * action create
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\Jihe $newJihe
	 * @return void
	 */
	public function createAction(\TaoJiang\MfwcJihe\Domain\Model\Jihe $newJihe) {
		$this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->jiheRepository->add($newJihe);
		$this->redirect('list');
	}

	/**
	 * action edit
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\Jihe $jihe
	 * @ignorevalidation $jihe
	 * @return void
	 */
	public function editAction(\TaoJiang\MfwcJihe\Domain\Model\Jihe $jihe) {
		$this->view->assign('jihe', $jihe);
	}

	/**
	 * action update
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\Jihe $jihe
	 * @return void
	 */
	public function updateAction(\TaoJiang\MfwcJihe\Domain\Model\Jihe $jihe) {
		$this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->jiheRepository->update($jihe);
		$this->redirect('list');
	}

	/**
	 * action delete
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\Jihe $jihe
	 * @return void
	 */
	public function deleteAction(\TaoJiang\MfwcJihe\Domain\Model\Jihe $jihe) {
		$this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See <a href="http://wiki.typo3.org/T3Doc/Extension_Builder/Using_the_Extension_Builder#1._Model_the_domain" target="_blank">Wiki</a>', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
		$this->jiheRepository->remove($jihe);
		$this->redirect('list');
	}

	/**
	 * action export
	 * 
	 * @return void
	 */
	public function exportAction() {
		
	}

	/**
	 * action public
	 * 
	 * @return void
	 */
	public function publicAction() {
		
	}

}
<?php
namespace TaoJiang\MfwcVolunteer\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ManagerUserController
 */
class ManagerUserController extends \TaoJiang\MfwcVolunteer\Controller\CommonController {

    
	/**
	 * 我的收藏列表
	 * 
	 * @return void
	 */
    /*
	public function favoriteListAction() {
    
        $mangerUser = $this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		$this->view->assign('volunteerUsers', $mangerUser->getTxMfwcvolunteerFavorite());
	}
    */
    
    
    /**
     * action 收藏
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
     * @return void
     */
    /*
    public function favoriteUpdateAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
        
        $mangerUser = $this->managerUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        //debug($mangerUser->getTxMfwcvolunteerFavorite(),'f');
        //exit;
        if($this->managerUserRepository->checkVolunteerFavoriate($volunteerUser)){
            
            $mangerUser->removeTxMfwcvolunteerFavorite($volunteerUser);
            $this->managerUserRepository->update($mangerUser);
            $this->addFlashMessage('取消收藏成功');
        }else{
            $mangerUser->addTxMfwcvolunteerFavorite($volunteerUser);
            $this->managerUserRepository->update($mangerUser);
            $this->addFlashMessage('收藏成功');
        }
        
        $this->redirect('list', 'VolunteerUser');
    }
    */
    
    /**
     * action 详细信息
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser
     * @return void
     */
    public function volunteerShowAction(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
    
        $this->view->assign('volunteerUser', $volunteerUser);
    }
}
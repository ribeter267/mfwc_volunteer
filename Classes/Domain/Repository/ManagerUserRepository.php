<?php
namespace TaoJiang\MfwcVolunteer\Domain\Repository;
class ManagerUserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
    
    /***
     * 所有管理者帐号
     * @param string departmentGroups
     */
    public function findDepartmentUsers($departmentGroups){
        
        $query = $this->createQuery();
        
        $groups = explode(',',$departmentGroups);
        $conditions = array();
        if(!empty($groups)){
            foreach($groups as $group){
                $conditions[] = $query->contains('usergroup',$group);
            }
        }
        $query->matching($query->logicalOr($conditions));
        //$GLOBALS['TYPO3_DB']->debugOutput = 2;
        $result = $query->execute();
        return $result;
    }
    
    
    /**
     * 检查是否已收藏
     */
    public function checkVolunteerFavoriate(\TaoJiang\MfwcVolunteer\Domain\Model\VolunteerUser $volunteerUser){
        
        $query = $this->createQuery();
        
        $query->matching($query->logicalAnd(array(
            $query->equals('uid',$GLOBALS['TSFE']->fe_user->user['uid']),
            $query->contains('txMfwcvolunteerFavorite',$volunteerUser)
        )));
        
        //$GLOBALS['TYPO3_DB']->debugOutput = 2;
        $result = $query->execute()->count();
        return $result;
    }
    
}
<?php
namespace TaoJiang\MfwcVolunteer\Domain\Repository;
class DepartmentRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	/**
	 * 根据部门管理员 获取当前所在部门
	 * @param int $user_id
	 * @return 
	 */
	public function findDepartmentByUserId($user_id){
	
		$query = $this->createQuery();
		//$query->getQuerySettings()->setRespectStoragePage(FALSE);
		
        $query->matching($query->equals('manager.managerUser.uid',$user_id));
		$query->setLimit(1);
		
		//$GLOBALS['TYPO3_DB']->debugOutput = 2;
		//debug($query->execute()->count(),'count');
        $result = $query->execute()->getFirst();
        return $result;
	}
	
	
	/**
	 * 根据职位 获取职位所在部门
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @return 
	 */
	public function findDepartmentByJob(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job){
	
		$query = $this->createQuery();
        $query->matching($query->contains('job',$job));
		$query->setLimit(1);
		
        $result = $query->execute()->getFirst();
        return $result;
	}
}
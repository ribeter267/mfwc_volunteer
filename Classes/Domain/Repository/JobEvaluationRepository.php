<?php
namespace TaoJiang\MfwcVolunteer\Domain\Repository;
class JobEvaluationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	public function findAll(){
	
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->setOrderings(array(
			'volunteeruser' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'fromtime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
		));
		
        $result = $query->execute();
        return $result;
	}
}
<?php
namespace TaoJiang\MfwcVolunteer\Domain\Repository;
class VolunteerUserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
    

    /**
     * 列出所有志愿者
     * @param string $volunteerGroup
     */
    public function findAllVolunteers($volunteerGroup, $keyword = ''){
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
		$query->getQuerySettings()->setRespectSysLanguage(FALSE);
        
        $groups = explode(',',$volunteerGroup);
        $conditions1 = array();
		//$conditions1[] = $query->logicalNot($query->equals('txMfwcvolunteerStatus',1));
        $conditions1[] = $query->logicalNot($query->equals('name', ''));
        
		$conditions = array(); 
		if(!empty($groups)){
            foreach($groups as $group){
                $conditions[] = $query->contains('usergroup',$group);
            }
        }
        $conditions1[] = $query->logicalOr($conditions);
        
        if($keyword != ''){
            $conditions1[] = $query->logicalOr(array(
                $query->like('txMfwcvolunteerId','%'.$keyword.'%'),
                $query->like('name','%'.$keyword.'%'),
                //$query->like('zone.title','%'.$keyword.'%'),
                //$query->like('city.title','%'.$keyword.'%'),
                $query->like('txMfwcvolunteerJobskeep','%'.$keyword.'%')
            )); 
        }
        
       // $GLOBALS['TYPO3_DB']->debugOutput = 2;
        
        $query->matching($query->logicalAnd($conditions1));
		$query->setOrderings(array(
			'txMfwcvolunteerStatus' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
			'txMfwcvolunteerId' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
		));
		
        $result = $query->execute();
        return $result;
    }
    
	
	 /**
     * 列出工作状态志愿者
     * @param string $volunteerGroup
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @param  string $keyword
     */
    public function findWorkingVolunteers($volunteerGroup, $keyword = ''){
        $query = $this->createQuery();
   
        $groups = explode(',',$volunteerGroup);
        $conditions1 = array();
		$conditions1[] = $query->equals('txMfwcvolunteerStatus',1);
		
		$conditions = array();
        if(!empty($groups)){
            foreach($groups as $group){
                $conditions[] = $query->contains('usergroup',$group);
            }
        }
        $conditions1[] = $query->logicalOr($conditions);
        
        if($keyword != ''){
            $conditions1[] = $query->logicalOr(array(
                $query->like('txMfwcvolunteerId','%'.$keyword.'%'),
                $query->like('name','%'.$keyword.'%'),
                //$query->like('txMfwcvolunteerWorkjobs.title','%'.$keyword.'%')
                $query->like('txMfwcvolunteerWorkjobskeep','%'.$keyword.'%')
            )); 
        }
        
		//$GLOBALS['TYPO3_DB']->debugOutput = 2;
		
        $query->matching($query->logicalAnd($conditions1));
		$query->setOrderings(array('txMfwcvolunteerId' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING));
		
        $result = $query->execute();
        return $result;
    }
	
	 /**
     * 列出本部门志愿者
     * @param string $volunteerGroup
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @param  string $keyword
     */
    public function findDepartmentVolunteers($volunteerGroup, $department, $keyword = ''){
        $query = $this->createQuery();
   
        $groups = explode(',',$volunteerGroup);
        $conditions1 = array();
		$conditions1[] = $query->equals('txMfwcvolunteerStatus',1);
		$conditions1[] = $query->equals('txMfwcvolunteerWorkdepartment',$department);
		
		$conditions = array();
        if(!empty($groups)){
            foreach($groups as $group){
                $conditions[] = $query->contains('usergroup',$group);
            }
        }
        $conditions1[] = $query->logicalOr($conditions);
        
        if($keyword != ''){
            $conditions1[] = $query->logicalOr(array(
                $query->like('txMfwcvolunteerId','%'.$keyword.'%'),
                $query->like('name','%'.$keyword.'%'),
                //$query->like('txMfwcvolunteerWorkjobs.title','%'.$keyword.'%')
                $query->like('txMfwcvolunteerWorkjobskeep','%'.$keyword.'%')
            )); 
        }
        
		//$GLOBALS['TYPO3_DB']->debugOutput = 2;
		
        $query->matching($query->logicalAnd($conditions1));
		
		$query->setOrderings(array('txMfwcvolunteerId' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING));
		
        $result = $query->execute();
        return $result;
    }
	
    
    /**
     * 显示所有列评价列表
     */
    public function findAllEvaluation($usergroup, $keyword = ''){
    
        $query = $this->createQuery();
        
        $conditions1 = array(
            $query->contains('usergroup', $usergroup),
            $query->greaterThan('txMfwcvolunteerEvaluation',0),
			$query->greaterThan('txMfwcvolunteerEvaluationLength',0),
        );
        
        
        if($keyword != ''){
            $conditions1[] = $query->logicalOr(array(
                $query->like('txMfwcvolunteerId','%'.$keyword.'%'),
                $query->like('name','%'.$keyword.'%'),
               // $query->like('zone.title','%'.$keyword.'%'),
                //$query->like('city.title','%'.$keyword.'%'),
                $query->like('txMfwcvolunteerWorkdepartment.title','%'.$keyword.'%'),
                //$query->like('txMfwcvolunteerWorkjobs.title','%'.$keyword.'%')
                $query->like('txMfwcvolunteerWorkjobskeep','%'.$keyword.'%')
            )); 
        }
        
        //$GLOBALS['TYPO3_DB']->debugOutput = 2;
        
        $query->matching($query->logicalAnd($conditions1));
		$query->setOrderings(array('txMfwcvolunteerId' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING));
		
        $result = $query->execute();
        return $result;
    }
    
    
    /***
     * 检查时间内是否存在记录
     * @param integer $volunteer_id
     * @param Datetime $fromTime
     * @param Datetime $toTime
     * @param int $evaluation_id 
     * 
     * @return integer
     */
    public function findExsitEvaluation($volunteer_id,$fromTime,$toTime,$evaluation_id = 0){
    
        $query = $this->createQuery();
        $conditions = array(
            $query->equals('uid',$volunteer_id),
            $query->logicalOr(
                $query->logicalAnd(array(
                    $query->greaterThanOrEqual('txMfwcvolunteerEvaluation.fromtime',$fromTime),
                    $query->lessThanOrEqual('txMfwcvolunteerEvaluation.fromtime',$toTime)
                )),
                $query->logicalAnd(array(
                    $query->greaterThanOrEqual('txMfwcvolunteerEvaluation.totime',$fromTime),
                    $query->lessThanOrEqual('txMfwcvolunteerEvaluation.totime',$toTime)
                ))
        ));
        
        if($evaluation_id > 0){
            $conditions[] = $query->logicalNot(
                $query->equals('txMfwcvolunteerEvaluation.uid',$evaluation_id)
            );
        }
        
        //$GLOBALS['TYPO3_DB']->debugOutput = 2;
        
        $query->matching($query->logicalAnd($conditions));
        $result = $query->execute()->count();
        return $result;
    }
    
    
    /**
     * 更新KEEP的部门
     * 
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
     * @return void
     */
    public function updateKeepDepartment(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department){
    
        $this->updateVolunteerDepartmentKeep($department, 'tx_mfwcvolunteer_department');
        $this->updateVolunteerDepartmentKeep($department, 'tx_mfwcvolunteer_workdepartment');
    }
    
    
     /**
     * 更新KEEP的部门
     * 
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
     * @param string $file
     * @return void
     */
    protected function updateVolunteerDepartmentKeep(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department, $file = 'tx_mfwcvolunteer_department'){
        
        $res =  $GLOBALS['TYPO3_DB']->exec_SELECTquery('uid, tx_mfwcvolunteer_jobskeep', 'fe_users', $file.'='.$department->getUid());
		if (!$GLOBALS['TYPO3_DB']->sql_error()) {
            while ($tempRow = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                
                $jobskeep = explode('-', $tempRow['tx_mfwcvolunteer_jobskeep']);
                $jobskeep[0] = $department->getTitle();
                
                $tempJobskeep = implode('-', $jobskeep);
                $GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'uid='.$tempRow['uid'], array('tx_mfwcvolunteer_jobskeep' => $tempJobskeep));
            }
		}
        $GLOBALS['TYPO3_DB']->sql_free_result($res);
    }
    
    
    /**
     * 更新KEEP的职位
     * 
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
     * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
     * @return void
     */
    public function updateKeepJob(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department, \TaoJiang\MfwcVolunteer\Domain\Model\Job $job){
        
        $GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'tx_mfwcvolunteer_jobs='.$job->getUid(), array('tx_mfwcvolunteer_jobskeep' => $department->getTitle() .' - '. $job->getTitle()));
        $GLOBALS['TYPO3_DB']->exec_UPDATEquery('fe_users', 'tx_mfwcvolunteer_workjobs='.$job->getUid(), array('tx_mfwcvolunteer_workjobskeep' => $department->getTitle() .' - '. $job->getTitle()));
    }
    
}
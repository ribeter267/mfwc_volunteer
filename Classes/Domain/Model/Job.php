<?php
namespace TaoJiang\MfwcVolunteer\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 职位
 */
class Job extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 岗位名称
	 * 
	 * @var string
	 */
	protected $title = '';

	/**
	 * 岗位级别
	 * 
	 * @var integer
	 */
	protected $level = 0;

	/**
	 * 岗位类别
	 * 
	 * @var integer
	 */
	protected $type = 0;

	/**
	 * 岗位描述
	 * 
	 * @var string
	 */
	protected $description = '';

	/**
	 * 岗位目标
	 * 
	 * @var string
	 */
	protected $goal = '';

	/**
	 * 任职资格
	 * 
	 * @var string
	 */
	protected $qualifications = '';

	/**
	 * 上级主管
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Competent
	 */
	protected $competent = NULL;


	/**
	 * Returns the title
	 * 
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the level
	 * 
	 * @return integer $level
	 */
	public function getLevel() {
		return $this->level;
	}

	/**
	 * Sets the level
	 * 
	 * @param integer $level
	 * @return void
	 */
	public function setLevel($level) {
		$this->level = $level;
	}

	/**
	 * Returns the type
	 * 
	 * @return integer $type
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * Sets the type
	 * 
	 * @param integer $type
	 * @return void
	 */
	public function setType($type) {
		$this->type = $type;
	}

	/**
	 * Returns the description
	 * 
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 * 
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the goal
	 * 
	 * @return string $goal
	 */
	public function getGoal() {
		return $this->goal;
	}

	/**
	 * Sets the goal
	 * 
	 * @param string $goal
	 * @return void
	 */
	public function setGoal($goal) {
		$this->goal = $goal;
	}

	/**
	 * Returns the qualifications
	 * 
	 * @return string $qualifications
	 */
	public function getQualifications() {
		return $this->qualifications;
	}

	/**
	 * Sets the qualifications
	 * 
	 * @param string $qualifications
	 * @return void
	 */
	public function setQualifications($qualifications) {
		$this->qualifications = $qualifications;
	}

	/**
	 * Returns the competent
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Competent $competent
	 */
	public function getCompetent() {
		return $this->competent;
	}

	/**
	 * Sets the competent
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Competent $competent
	 * @return void
	 */
	public function setCompetent(\TaoJiang\MfwcVolunteer\Domain\Model\Competent $competent) {
		$this->competent = $competent;
	}


}
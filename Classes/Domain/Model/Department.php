<?php
namespace TaoJiang\MfwcVolunteer\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 部门
 */
class Department extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 部门名称
	 * 
	 * @var string
	 */
	protected $title = '';

	/**
	 * 部门管理员
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager>
	 * @cascade remove
	 */
	protected $manager = NULL;
    
    
    /**
	 * 工作岗位
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\Job>
	 * @cascade remove
	 */
	protected $job = NULL;
    

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 * 
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->manager = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 * 
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Adds a DepartmentManager
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager $manager
	 * @return void
	 */
	public function addManager(\TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager $manager) {
		$this->manager->attach($manager);
	}

	/**
	 * Removes a DepartmentManager
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager $managerToRemove The DepartmentManager to be removed
	 * @return void
	 */
	public function removeManager(\TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager $managerToRemove) {
		$this->manager->detach($managerToRemove);
	}

	/**
	 * Returns the manager
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager> $manager
	 */
	public function getManager() {
		return $this->manager;
	}

	/**
	 * Sets the manager
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\DepartmentManager> $manager
	 * @return void
	 */
	public function setManager(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $manager) {
		$this->manager = $manager;
	}
    
    
    /**
	 * Adds a Job
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @return void
	 */
	public function addJob(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job) {
		$this->job->attach($job);
	}

	/**
	 * Removes a Job
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $jobToRemove The Job to be removed
	 * @return void
	 */
	public function removeJob(\TaoJiang\MfwcVolunteer\Domain\Model\Job $jobToRemove) {
		$this->job->detach($jobToRemove);
	}

	/**
	 * Returns the manager
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\Job> $job
	 */
	public function getJob() {
		return $this->job;
	}

	/**
	 * Sets the job
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\Job> $job
	 * @return void
	 */
	public function setJob(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $job) {
		$this->job = $job;
	}

}
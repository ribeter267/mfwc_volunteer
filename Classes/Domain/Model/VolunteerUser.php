<?php
namespace TaoJiang\MfwcVolunteer\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * VolunteerUser
 */
class VolunteerUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * 志愿者编号
	 * 
	 * @var string
	 */
	protected $txMfwcvolunteerId = '';
    
    
    /**
     * 性别
     * @var integer
	 */
    protected $gender = 0;
    
    /**
     * 省份
     * @var \TaoJiang\MfwcVolunteer\Domain\Model\Zone
	 */
    protected $zone = NULL;
    
    
    /**
     * 城市
     * @var \TaoJiang\MfwcVolunteer\Domain\Model\City
	 */
    protected $city = NULL;
    
    
    /**
     * 生日
     * @var \DateTime
	 */
    protected $birthday = NULL;
    
    
    /**
	 * 志愿者微博
	 * 
	 * @var string
	 */
	protected $txMfwcvolunteerWeibo = '';
    
    /**
	 * 个人简介
	 * 
	 * @var string
	 */
	protected $comments = '';
    

	/**
	 * 状态
	 * 
	 * @var integer
	 */
	protected $txMfwcvolunteerStatus = 0;
    
    

    /**
	 * 认证时间
	 * 
	 * @var \DateTime
	 */
	protected $txMfwcvolunteerAuthenticationTime = NULL;

    
	/**
	 * 是否有志愿者经验
	 * 
	 * @var boolean
	 */
	protected $txMfwcvolunteerExperience = FALSE;

	/**
	 * 技能专长
	 * 
	 * @var string
	 */
	protected $txMfwcvolunteerSkills = '';

	/**
	 * 工作职责说明
	 * 
	 * @var string
	 */
	protected $txMfwcvolunteerResponsibilities = '';
    
    
    /**
	 * 希望加入的志愿者工作岗位
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Department
	 */
	protected $txMfwcvolunteerDepartment = NULL;
    

	/**
	 * 希望加入的志愿者工作岗位
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Job
	 */
	protected $txMfwcvolunteerJobs = NULL;
    
    
    /**
	 * 工作部门
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Department
	 */
	protected $txMfwcvolunteerWorkdepartment = NULL;
    
    /**
	 * 工作岗位
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Job
	 */
	protected $txMfwcvolunteerWorkjobs = NULL;
    
    
	/**
	 * 工作评价
	 * 
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation>
	 * @cascade remove
	 */
	protected $txMfwcvolunteerEvaluation = NULL;
    
    
    /**
     * 工作总时长
     * @var \float
	 */
	protected $txMfwcvolunteerEvaluationLength = 0.00;
    
    
	/**
     * 评价总状态
     * @var \float
	 */
	protected $txMfwcvolunteerEvaluationStatus = 0;
	
    /**
	 * QQ
	 * 
	 * @var string
	 */
    protected $txMfwcshoolinfosQq = '';
    
    
    /**
	 * 微博
	 * 
	 * @var string
	 */
    protected $txMfwcshoolinfosWeibo = '';
	
	
	/**
	 * 管理人
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser
	 */
	protected $txMfwcvolunteerVolunteermanager = NULL;
	
	
	/**
	 * 加入岗位(记录)
	 * 
	 * @var string
	 */
    protected $txMfwcvolunteerJobskeep = '';
	
	
	/**
	 * 工作岗位(记录)
	 * 
	 * @var string
	 */
    protected $txMfwcvolunteerWorkjobskeep = '';
    
	
	/**
	 * 审核评定
	 * 
	 * @var string
	 */
	protected $assessment = '';

	/**
	 * Returns the assessment
	 * 
	 * @return string $assessment
	 */
	public function getAssessment() {
		return $this->assessment;
	}

	/**
	 * Sets the assessment
	 * 
	 * @param string $assessment
	 * @return void
	 */
	public function setAssessment($assessment) {
		$this->assessment = $assessment;
	}
	

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 * 
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->txMfwcvolunteerEvaluation = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the txMfwcvolunteerId
	 * 
	 * @return string $txMfwcvolunteerId
	 */
	public function getTxMfwcvolunteerId() {
		return $this->txMfwcvolunteerId;
	}

	/**
	 * Sets the txMfwcvolunteerId
	 * 
	 * @param string $txMfwcvolunteerId
	 * @return void
	 */
	public function setTxMfwcvolunteerId($txMfwcvolunteerId) {
		$this->txMfwcvolunteerId = $txMfwcvolunteerId;
	}
    
    
    
    /**
	 * Returns the Zone
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Zone $zone
	 */
	public function getZone() {
		return $this->zone;
	}

	/**
	 * Sets the zone
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Zone $zone
	 * @return void
	 */
	public function setZone(\TaoJiang\MfwcVolunteer\Domain\Model\Zone $zone) {
		$this->zone = $zone;
	}
    
    
    
    /**
	 * Returns the City
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\City $city
	 */
	public function getCity() {
		return $this->city;
	}

    
	/**
	 * Sets the city
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\City $city
	 * @return void
	 */
	public function setCity(\TaoJiang\MfwcVolunteer\Domain\Model\City $city) {
		$this->city = $city;
	}
    
    

	/**
	 * Returns the txMfwcvolunteerStatus
	 * 
	 * @return integer $txMfwcvolunteerStatus
	 */
	public function getTxMfwcvolunteerStatus() {
		return $this->txMfwcvolunteerStatus;
	}

	/**
	 * Sets the txMfwcvolunteerStatus
	 * 
	 * @param integer $txMfwcvolunteerStatus
	 * @return void
	 */
	public function setTxMfwcvolunteerStatus($txMfwcvolunteerStatus) {
		$this->txMfwcvolunteerStatus = $txMfwcvolunteerStatus;
	}

	/**
	 * Returns the txMfwcvolunteerExperience
	 * 
	 * @return boolean $txMfwcvolunteerExperience
	 */
	public function getTxMfwcvolunteerExperience() {
		return $this->txMfwcvolunteerExperience;
	}

	/**
	 * Sets the txMfwcvolunteerExperience
	 * 
	 * @param boolean $txMfwcvolunteerExperience
	 * @return void
	 */
	public function setTxMfwcvolunteerExperience($txMfwcvolunteerExperience) {
		$this->txMfwcvolunteerExperience = $txMfwcvolunteerExperience;
	}

	/**
	 * Returns the boolean state of txMfwcvolunteerExperience
	 * 
	 * @return boolean
	 */
	public function isTxMfwcvolunteerExperience() {
		return $this->txMfwcvolunteerExperience;
	}

	/**
	 * Returns the txMfwcvolunteerSkills
	 * 
	 * @return string $txMfwcvolunteerSkills
	 */
	public function getTxMfwcvolunteerSkills() {
		return $this->txMfwcvolunteerSkills;
	}

	/**
	 * Sets the txMfwcvolunteerSkills
	 * 
	 * @param string $txMfwcvolunteerSkills
	 * @return void
	 */
	public function setTxMfwcvolunteerSkills($txMfwcvolunteerSkills) {
		$this->txMfwcvolunteerSkills = $txMfwcvolunteerSkills;
	}

	/**
	 * Returns the txMfwcvolunteerResponsibilities
	 * 
	 * @return string $txMfwcvolunteerResponsibilities
	 */
	public function getTxMfwcvolunteerResponsibilities() {
		return $this->txMfwcvolunteerResponsibilities;
	}

	/**
	 * Sets the txMfwcvolunteerResponsibilities
	 * 
	 * @param string $txMfwcvolunteerResponsibilities
	 * @return void
	 */
	public function setTxMfwcvolunteerResponsibilities($txMfwcvolunteerResponsibilities) {
		$this->txMfwcvolunteerResponsibilities = $txMfwcvolunteerResponsibilities;
	}

	/**
	 * Returns the txMfwcvolunteerJobs
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Job $txMfwcvolunteerJobs
	 */
	public function getTxMfwcvolunteerJobs() {
		return $this->txMfwcvolunteerJobs;
	}

	/**
	 * Sets the txMfwcvolunteerJobs
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $txMfwcvolunteerJobs
	 * @return void
	 */
	public function setTxMfwcvolunteerJobs(\TaoJiang\MfwcVolunteer\Domain\Model\Job $txMfwcvolunteerJobs) {
		$this->txMfwcvolunteerJobs = $txMfwcvolunteerJobs;
	}
    
    
    /**
	 * Returns the txMfwcvolunteerDepartment
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Department $txMfwcvolunteerDepartment
	 */
	public function getTxMfwcvolunteerDepartment() {
		return $this->txMfwcvolunteerDepartment;
	}

	/**
	 * Sets the txMfwcvolunteerDepartment
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $txMfwcvolunteerDepartment
	 * @return void
	 */
	public function setTxMfwcvolunteerDepartment(\TaoJiang\MfwcVolunteer\Domain\Model\Department $txMfwcvolunteerDepartment) {
		$this->txMfwcvolunteerDepartment = $txMfwcvolunteerDepartment;
	}

	
	
	/**
	 * Returns the txMfwcvolunteerWorkdepartment
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Department $txMfwcvolunteerWorkdepartment
	 */
	public function getTxMfwcvolunteerWorkdepartment() {
		return $this->txMfwcvolunteerWorkdepartment;
	}

	/**
	 * Sets the txMfwcvolunteerWorkdepartment
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $txMfwcvolunteerWorkdepartment
	 * @return void
	 */
	public function setTxMfwcvolunteerWorkdepartment(\TaoJiang\MfwcVolunteer\Domain\Model\Department $txMfwcvolunteerWorkdepartment) {
		$this->txMfwcvolunteerWorkdepartment = $txMfwcvolunteerWorkdepartment;
	}
	
	/**
	 * Sets the txMfwcvolunteerWorkdepartment
	 * @return void
	 */
	public function resetTxMfwcvolunteerWorkdepartment() {
		$this->txMfwcvolunteerWorkdepartment = null;
	}
	
	/**
	 * Returns the txMfwcvolunteerWorkjobs
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Job $txMfwcvolunteerWorkjobs
	 */
	public function getTxMfwcvolunteerWorkjobs() {
		return $this->txMfwcvolunteerWorkjobs;
	}

	/**
	 * Sets the txMfwcvolunteerWorkjobs
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $txMfwcvolunteerWorkjobs
	 * @return void
	 */
	public function setTxMfwcvolunteerWorkjobs(\TaoJiang\MfwcVolunteer\Domain\Model\Job $txMfwcvolunteerWorkjobs) {
		$this->txMfwcvolunteerWorkjobs = $txMfwcvolunteerWorkjobs;
	}
	
	/**
	 * Sets the txMfwcvolunteerWorkjobs
	 * @return void
	 */
	public function resetTxMfwcvolunteerWorkjobs() {
		$this->txMfwcvolunteerWorkjobs = null;
	}
	
	
	/**
	 * Adds a JobEvaluation
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $txMfwcvolunteerEvaluation
	 * @return void
	 */
	public function addTxMfwcvolunteerEvaluation(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $txMfwcvolunteerEvaluation) {
		$this->txMfwcvolunteerEvaluation->attach($txMfwcvolunteerEvaluation);
	}

	/**
	 * Removes a JobEvaluation
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $txMfwcvolunteerEvaluationToRemove The JobEvaluation to be removed
	 * @return void
	 */
	public function removeTxMfwcvolunteerEvaluation(\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation $txMfwcvolunteerEvaluationToRemove) {
		$this->txMfwcvolunteerEvaluation->detach($txMfwcvolunteerEvaluationToRemove);
	}

	/**
	 * Returns the txMfwcvolunteerEvaluation
	 * 
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation> $txMfwcvolunteerEvaluation
	 */
	public function getTxMfwcvolunteerEvaluation() {
		return $this->txMfwcvolunteerEvaluation;
	}

	/**
	 * Sets the txMfwcvolunteerEvaluation
	 * 
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TaoJiang\MfwcVolunteer\Domain\Model\JobEvaluation> $txMfwcvolunteerEvaluation
	 * @return void
	 */
	public function setTxMfwcvolunteerEvaluation(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $txMfwcvolunteerEvaluation) {
		$this->txMfwcvolunteerEvaluation = $txMfwcvolunteerEvaluation;
	}
    
    /**
	 * Returns the txMfwcvolunteerAuthenticationTime
	 * 
	 * @return \DateTime $txMfwcvolunteerAuthenticationTime
	 */
	public function getTxMfwcvolunteerAuthenticationTime() {
		return $this->txMfwcvolunteerAuthenticationTime;
	}

	/**
	 * Sets the txMfwcvolunteerAuthenticationTime
	 * 
	 * @param \DateTime $txMfwcvolunteerAuthenticationTime
	 * @return void
	 */
	public function setTxMfwcvolunteerAuthenticationTime(\DateTime $txMfwcvolunteerAuthenticationTime) {
		$this->txMfwcvolunteerAuthenticationTime = $txMfwcvolunteerAuthenticationTime;
	}
    
    /**
	 * Returns the txMfwcshoolinfosQq
	 * 
	 * @return string $txMfwcshoolinfosQq
	 */
	public function getTxMfwcshoolinfosQq() {
		return $this->txMfwcshoolinfosQq;
	}

	/**
	 * Sets the txMfwcshoolinfosQq
	 * 
	 * @param string $txMfwcshoolinfosQq
	 * @return void
	 */
	public function setTxMfwcshoolinfosQq($txMfwcshoolinfosQq) {
		$this->txMfwcshoolinfosQq = $txMfwcshoolinfosQq;
	}
    
    
    /**
	 * Returns the txMfwcshoolinfosWeibo
	 * 
	 * @return string $txMfwcshoolinfosWeibo
	 */
	public function getTxMfwcshoolinfosWeibo() {
		return $this->txMfwcshoolinfosWeibo;
	}

	/**
	 * Sets the txMfwcshoolinfosWeibo
	 * 
	 * @param string $txMfwcshoolinfosWeibo
	 * @return void
	 */
	public function setTxMfwcshoolinfosWeibo($txMfwcshoolinfosWeibo) {
		$this->txMfwcshoolinfosWeibo = $txMfwcshoolinfosWeibo;
	}
    
    /**
	 * Get gender
	 *
	 * @return integer
	 */
	public function getGender() {
		return $this->gender;
	}

    
	/**
	 * Set gender
	 *
	 * @param integer $gender 
	 * @return void
	 */
	public function setGender($gender) {
		$this->gender = $gender;
	}
    
    /**
	 * Returns the txMfwcvolunteerEvaluationLength
	 * 
	 * @return float $txMfwcvolunteerEvaluationLength
	 */
	public function getTxMfwcvolunteerEvaluationLength() {
		return $this->txMfwcvolunteerEvaluationLength;
	}

	/**
	 * Sets the txMfwcvolunteerEvaluationLength
	 * 
	 * @param float $txMfwcvolunteerEvaluationLength
	 * @return void
	 */
	public function setTxMfwcvolunteerEvaluationLength($txMfwcvolunteerEvaluationLength) {
		$this->txMfwcvolunteerEvaluationLength = $txMfwcvolunteerEvaluationLength;
	}
    
	
	/**
	 * Returns the txMfwcvolunteerEvaluationStatus
	 * 
	 * @return float $txMfwcvolunteerEvaluationStatus
	 */
	public function getTxMfwcvolunteerEvaluationStatus() {
		return $this->txMfwcvolunteerEvaluationStatus;
	}

	/**
	 * Sets the txMfwcvolunteerEvaluationStatus
	 * 
	 * @param float $txMfwcvolunteerEvaluationStatus
	 * @return void
	 */
	public function setTxMfwcvolunteerEvaluationStatus($txMfwcvolunteerEvaluationStatus) {
		$this->txMfwcvolunteerEvaluationStatus = $txMfwcvolunteerEvaluationStatus;
	}
	
	
    /**
	 * Returns the birthday
	 * 
	 * @return \DateTime $birthday
	 */
	public function getBirthday() {
		return $this->birthday;
	}

	/**
	 * Sets the birthday
	 * 
	 * @param \DateTime $birthday
	 * @return void
	 */
	public function setBirthday(\DateTime $birthday) {
		$this->birthday = $birthday;
	}

    /**
	 * Returns the txMfwcvolunteerWeibo
	 * 
	 * @return string $txMfwcvolunteerWeibo
	 */
	public function getTxMfwcvolunteerWeibo() {
		return $this->txMfwcvolunteerWeibo;
	}

	/**
	 * Sets the txMfwcvolunteerWeibo
	 * 
	 * @param string $txMfwcvolunteerWeibo
	 * @return void
	 */
	public function setTxMfwcvolunteerWeibo($txMfwcvolunteerWeibo) {
		$this->txMfwcvolunteerWeibo = $txMfwcvolunteerWeibo;
	}
    
     /**
	 * Returns the comments
	 * 
	 * @return string $comments
	 */
	public function getComments() {
		return $this->comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param string $comments
	 * @return void
	 */
	public function setComments($comments) {
		$this->comments = $comments;
	}
	
	/**
	 * Returns the txMfwcvolunteerVolunteermanager
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $txMfwcvolunteerVolunteermanager
	 */
	public function getTxMfwcvolunteerVolunteermanager() {
		return $this->txMfwcvolunteerVolunteermanager;
	}

	/**
	 * Sets the txMfwcvolunteerVolunteermanager
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $txMfwcvolunteerVolunteermanager
	 * @return void
	 */
	public function setTxMfwcvolunteerVolunteermanager(\TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $txMfwcvolunteerVolunteermanager) {
		$this->txMfwcvolunteerVolunteermanager = $txMfwcvolunteerVolunteermanager;
	}
	
	
	/**
	 * Sets the txMfwcvolunteerVolunteermanager
	 * @return void
	 */
	public function resetTxMfwcvolunteerVolunteermanager() {
		$this->txMfwcvolunteerVolunteermanager = null;
	}
	
	/**
	 * Returns the txMfwcvolunteerJobskeep
	 * 
	 * @return string $txMfwcvolunteerJobskeep
	 */
	public function getTxMfwcvolunteerJobskeep() {
		return $this->txMfwcvolunteerJobskeep;
	}

	/**
	 * Sets the txMfwcvolunteerJobskeep
	 * 
	 * @param string $txMfwcvolunteerJobskeep
	 * @return void
	 */
	public function setTxMfwcvolunteerJobskeep($txMfwcvolunteerJobskeep) {
		$this->txMfwcvolunteerJobskeep = $txMfwcvolunteerJobskeep;
	}
    
	/**
	 * Returns the txMfwcvolunteerWorkjobskeep
	 * 
	 * @return string $txMfwcvolunteerWorkjobskeep
	 */
	public function getTxMfwcvolunteerWorkjobskeep() {
		return $this->txMfwcvolunteerWorkjobskeep;
	}
	

	/**
	 * Sets the txMfwcvolunteerWorkjobskeep
	 * 
	 * @param string $txMfwcvolunteerWorkjobskeep
	 * @return void
	 */
	public function setTxMfwcvolunteerWorkjobskeep($txMfwcvolunteerWorkjobskeep) {
		$this->txMfwcvolunteerWorkjobskeep = $txMfwcvolunteerWorkjobskeep;
	}
	
	/**
	 * Sets the txMfwcvolunteerWorkjobskeep
	 * @return void
	 */
	public function resetTxMfwcvolunteerWorkjobskeep() {
		$this->txMfwcvolunteerWorkjobskeep = '';
	}
    
}

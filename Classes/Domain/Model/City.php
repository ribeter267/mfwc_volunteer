<?php
namespace TaoJiang\MfwcVolunteer\Domain\Model;
class City extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 名称
	 * 
	 * @var string
	 */
	protected $title = '';

	/**
	 * Returns the title
	 * 
	 * @return string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 * 
	 * @param string $title
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

}
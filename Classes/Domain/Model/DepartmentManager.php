<?php
namespace TaoJiang\MfwcVolunteer\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 部门管理人
 */
class DepartmentManager extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 是否邮件通知
	 * 
	 * @var boolean
	 */
	protected $sendemail = FALSE;

	/**
	 * 对应用户
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser
	 */
	protected $managerUser = NULL;

	/**
	 * Returns the sendemail
	 * 
	 * @return boolean $sendemail
	 */
	public function getSendemail() {
		return $this->sendemail;
	}

	/**
	 * Sets the sendemail
	 * 
	 * @param boolean $sendemail
	 * @return void
	 */
	public function setSendemail($sendemail) {
		$this->sendemail = $sendemail;
	}

	/**
	 * Returns the boolean state of sendemail
	 * 
	 * @return boolean
	 */
	public function isSendemail() {
		return $this->sendemail;
	}

	/**
	 * Returns the managerUser
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $managerUser
	 */
	public function getManagerUser() {
		return $this->managerUser;
	}

	/**
	 * Sets the managerUser
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $managerUser
	 * @return void
	 */
	public function setManagerUser(\TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $managerUser) {
		$this->managerUser = $managerUser;
	}

}
<?php
namespace TaoJiang\MfwcVolunteer\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 TaoJIang <ribeter267@gmail.com>, www.mianfeiwucan.org
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 工作评价
 */
class JobEvaluation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
	 * @var integer
	 */
	protected $crdate;
    
    
    /**
	 * @var integer
	 */
	protected $tstamp;
    
    
    /**
	 * 对应用户
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser
	 */
	protected $recorder = NULL;
    

	/**
	 * 工作评价
	 * 
	 * @var integer
	 * @validate NotEmpty
	 */
	protected $evluation = 0;

	/**
	 * 工作内容
	 * 
	 * @var string
	 * @validate NotEmpty
	 */
	protected $description = '';

	/**
	 * 开始时间
	 * @var \DateTime
	 * @validate NotEmpty DateTime
	 */
	protected $fromtime = NULL;

	/**
	 * 结束时间
	 * @var \DateTime
	 * @validate NotEmpty DateTime
	 */
	protected $totime = NULL;
    
    
    /**
     * 时长
     * @var \float
	 */
	protected $length = 0; 
    
	
	/**
	 * 工作岗位
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Department
	 */
	protected $department = NULL;
    

	/**
	 * 工作岗位
	 * 
	 * @var \TaoJiang\MfwcVolunteer\Domain\Model\Job
	 */
	protected $job = NULL;
	
	/**
	 * 工作岗位
	 * 
	 * @var \string
	 */
	protected $jobkeep = NULL;
	
    
    
    /**
	 * Get creation date
	 *
	 * @return integer
	 */
	public function getCrdate() {
		return $this->crdate;
	}

	/**
	 * Set creation date
	 *
	 * @param integer $crdate
	 * @return void
	 */
	public function setCrdate($crdate) {
		$this->crdate = $crdate;
	}
    
    
    /**
	 * Get timestamp
	 *
	 * @return integer
	 */
	public function getTstamp() {
		return $this->tstamp;
	}

	/**
	 * Set time stamp
	 *
	 * @param integer $tstamp time stamp
	 * @return void
	 */
	public function setTstamp($tstamp) {
		$this->tstamp = $tstamp;
	}
    

	/**
	 * Returns the evluation
	 * 
	 * @return integer $evluation
	 */
	public function getEvluation() {
		return $this->evluation;
	}

	/**
	 * Sets the evluation
	 * 
	 * @param integer $evluation
	 * @return void
	 */
	public function setEvluation($evluation) {
		$this->evluation = $evluation;
	}

	/**
	 * Returns the description
	 * 
	 * @return string $description
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Sets the description
	 * 
	 * @param string $description
	 * @return void
	 */
	public function setDescription($description) {
		$this->description = $description;
	}

	/**
	 * Returns the fromtime
	 * 
	 * @return \DateTime $fromtime
	 */
	public function getFromtime() {
		return $this->fromtime;
	}

	/**
	 * Sets the fromtime
	 * 
	 * @param \DateTime $fromtime
	 * @return void
	 */
	public function setFromtime(\DateTime $fromtime) {
		$this->fromtime = $fromtime;
	}

	/**
	 * Returns the totime
	 * 
	 * @return \DateTime $totime
	 */
	public function getTotime() {
		return $this->totime;
	}

	/**
	 * Sets the totime
	 * 
	 * @param \DateTime $totime
	 * @return void
	 */
	public function setTotime(\DateTime $totime) {
		$this->totime = $totime;
	}
    
    /**
	 * Returns the length
	 * 
	 * @return float $length
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * Sets the length
	 * 
	 * @param float $length
	 * @return void
	 */
	public function setLength($length) {
		$this->length = $length;
	}
    
    /**
	 * Returns the recorder
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $recorder
	 */
	public function getRecorder() {
		return $this->recorder;
	}
    

	/**
	 * Sets the recorder
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $recorder
	 * @return void
	 */
	public function setRecorder(\TaoJiang\MfwcVolunteer\Domain\Model\ManagerUser $recorder) {
		$this->recorder = $recorder;
	}
	
	/**
	 * Returns the job
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 */
	public function getJob() {
		return $this->job;
	}

	/**
	 * Sets the job
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Job $job
	 * @return void
	 */
	public function setJob(\TaoJiang\MfwcVolunteer\Domain\Model\Job $job) {
		$this->job = $job;
	}
    
    
    /**
	 * Returns the department
	 * 
	 * @return \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * Sets the department
	 * 
	 * @param \TaoJiang\MfwcVolunteer\Domain\Model\Department $department
	 * @return void
	 */
	public function setDepartment(\TaoJiang\MfwcVolunteer\Domain\Model\Department $department) {
		$this->department = $department;
	}

	
	/**
	 * Returns the jobkeep
	 * 
	 * @return $jobkeep
	 */
	public function getJobkeep() {
		return $this->jobkeep;
	}

	/**
	 * Sets the jobkeep
	 * 
	 * @param \string $jobkeep
	 * @return void
	 */
	public function setJobkeep( $jobkeep) {
		$this->jobkeep = $jobkeep;
	}

}
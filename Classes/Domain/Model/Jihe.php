<?php
namespace TaoJiang\MfwcJihe\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 稽核信息
 */
class Jihe extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * 稽核时间
	 * 
	 * @var \DateTime
	 */
	protected $jiheDate = NULL;

	/**
	 * 稽核人
	 * 
	 * @var string
	 */
	protected $jiher = '';

	/**
	 * 稽核报告
	 * 
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $jiheReport = NULL;

	/**
	 * 评分
	 * 
	 * @var int
	 */
	protected $score = 0;

	/**
	 * 反馈时间
	 * 
	 * @var \DateTime
	 */
	protected $feedbackDate = NULL;

	/**
	 * 稽核反馈报告
	 * 
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $feedbackReport = NULL;

	/**
	 * 学校
	 * 
	 * @var \TaoJiang\MfwcJihe\Domain\Model\School
	 */
	protected $school = NULL;

	/**
	 * Returns the jiheDate
	 * 
	 * @return \DateTime $jiheDate
	 */
	public function getJiheDate() {
		return $this->jiheDate;
	}

	/**
	 * Sets the jiheDate
	 * 
	 * @param \DateTime $jiheDate
	 * @return void
	 */
	public function setJiheDate(\DateTime $jiheDate) {
		$this->jiheDate = $jiheDate;
	}

	/**
	 * Returns the jiher
	 * 
	 * @return string $jiher
	 */
	public function getJiher() {
		return $this->jiher;
	}

	/**
	 * Sets the jiher
	 * 
	 * @param string $jiher
	 * @return void
	 */
	public function setJiher($jiher) {
		$this->jiher = $jiher;
	}

	/**
	 * Returns the jiheReport
	 * 
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $jiheReport
	 */
	public function getJiheReport() {
		return $this->jiheReport;
	}

	/**
	 * Sets the jiheReport
	 * 
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $jiheReport
	 * @return void
	 */
	public function setJiheReport(\TYPO3\CMS\Extbase\Domain\Model\FileReference $jiheReport) {
		$this->jiheReport = $jiheReport;
	}

	/**
	 * Returns the score
	 * 
	 * @return int $score
	 */
	public function getScore() {
		return $this->score;
	}

	/**
	 * Sets the score
	 * 
	 * @param int $score
	 * @return void
	 */
	public function setScore($score) {
		$this->score = $score;
	}

	/**
	 * Returns the feedbackDate
	 * 
	 * @return \DateTime $feedbackDate
	 */
	public function getFeedbackDate() {
		return $this->feedbackDate;
	}

	/**
	 * Sets the feedbackDate
	 * 
	 * @param \DateTime $feedbackDate
	 * @return void
	 */
	public function setFeedbackDate(\DateTime $feedbackDate) {
		$this->feedbackDate = $feedbackDate;
	}

	/**
	 * Returns the feedbackReport
	 * 
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $feedbackReport
	 */
	public function getFeedbackReport() {
		return $this->feedbackReport;
	}

	/**
	 * Sets the feedbackReport
	 * 
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $feedbackReport
	 * @return void
	 */
	public function setFeedbackReport(\TYPO3\CMS\Extbase\Domain\Model\FileReference $feedbackReport) {
		$this->feedbackReport = $feedbackReport;
	}

	/**
	 * Returns the school
	 * 
	 * @return \TaoJiang\MfwcJihe\Domain\Model\School $school
	 */
	public function getSchool() {
		return $this->school;
	}

	/**
	 * Sets the school
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\School $school
	 * @return void
	 */
	public function setSchool(\TaoJiang\MfwcJihe\Domain\Model\School $school) {
		$this->school = $school;
	}

}
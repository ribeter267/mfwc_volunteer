<?php
namespace TaoJiang\MfwcJihe\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * 学校信息
 */
class School extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

	/**
	 * 学校编号
	 * 
	 * @var string
	 */
	protected $txMfwcshoolinfosId = '';

	/**
	 * 微博
	 * 
	 * @var string
	 */
	protected $txMfwcshoolinfosWeibo = '';

	/**
	 * 执行区域
	 * 
	 * @var \TaoJiang\MfwcJihe\Domain\Model\Area
	 */
	protected $txMfwcshoolinfosArea = NULL;

	/**
	 * 集合分组
	 * 
	 * @var \TaoJiang\MfwcJihe\Domain\Model\Group
	 */
	protected $jiheGroup = NULL;

	/**
	 * Returns the txMfwcshoolinfosId
	 * 
	 * @return string $txMfwcshoolinfosId
	 */
	public function getTxMfwcshoolinfosId() {
		return $this->txMfwcshoolinfosId;
	}

	/**
	 * Sets the txMfwcshoolinfosId
	 * 
	 * @param string $txMfwcshoolinfosId
	 * @return void
	 */
	public function setTxMfwcshoolinfosId($txMfwcshoolinfosId) {
		$this->txMfwcshoolinfosId = $txMfwcshoolinfosId;
	}

	/**
	 * Returns the txMfwcshoolinfosWeibo
	 * 
	 * @return string $txMfwcshoolinfosWeibo
	 */
	public function getTxMfwcshoolinfosWeibo() {
		return $this->txMfwcshoolinfosWeibo;
	}

	/**
	 * Sets the txMfwcshoolinfosWeibo
	 * 
	 * @param string $txMfwcshoolinfosWeibo
	 * @return void
	 */
	public function setTxMfwcshoolinfosWeibo($txMfwcshoolinfosWeibo) {
		$this->txMfwcshoolinfosWeibo = $txMfwcshoolinfosWeibo;
	}

	/**
	 * Returns the txMfwcshoolinfosArea
	 * 
	 * @return \TaoJiang\MfwcJihe\Domain\Model\Area $txMfwcshoolinfosArea
	 */
	public function getTxMfwcshoolinfosArea() {
		return $this->txMfwcshoolinfosArea;
	}

	/**
	 * Sets the txMfwcshoolinfosArea
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\Area $txMfwcshoolinfosArea
	 * @return void
	 */
	public function setTxMfwcshoolinfosArea(\TaoJiang\MfwcJihe\Domain\Model\Area $txMfwcshoolinfosArea) {
		$this->txMfwcshoolinfosArea = $txMfwcshoolinfosArea;
	}

	/**
	 * Returns the jiheGroup
	 * 
	 * @return \TaoJiang\MfwcJihe\Domain\Model\Group $jiheGroup
	 */
	public function getJiheGroup() {
		return $this->jiheGroup;
	}

	/**
	 * Sets the jiheGroup
	 * 
	 * @param \TaoJiang\MfwcJihe\Domain\Model\Group $jiheGroup
	 * @return void
	 */
	public function setJiheGroup(\TaoJiang\MfwcJihe\Domain\Model\Group $jiheGroup) {
		$this->jiheGroup = $jiheGroup;
	}

}
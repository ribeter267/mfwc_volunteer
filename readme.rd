############################################### 
####            清理数据库              #######
############################################### 
truncate table tx_mfwcvolunteer_domain_model_jobevaluation;
UPDATE fe_users SET tx_mfwcvolunteer_evaluation_length = 0;


############################################### 
####          更新已有的数据            #######
############################################### 
SELECT usergroup, tx_mfwcvolunteer_department, tx_mfwcvolunteer_jobs, tx_mfwcvolunteer_jobskeep, tx_mfwcvolunteer_workdepartment, tx_mfwcvolunteer_workjobs, tx_mfwcvolunteer_workjobskeep
FROM  `fe_users` 
WHERE FIND_IN_SET( 2, usergroup );

UPDATE fe_users f, tx_mfwcvolunteer_domain_model_department d, tx_mfwcvolunteer_domain_model_job j
SET f.tx_mfwcvolunteer_jobskeep = concat(d.title,' - ',j.title)
WHERE  FIND_IN_SET( 2, f.usergroup ) AND f.tx_mfwcvolunteer_department = d.uid AND f.tx_mfwcvolunteer_jobs = j.uid;

UPDATE fe_users f, tx_mfwcvolunteer_domain_model_department d, tx_mfwcvolunteer_domain_model_job j
SET f.tx_mfwcvolunteer_workjobskeep = concat(d.title,' - ',j.title)
WHERE  FIND_IN_SET( 2, f.usergroup ) AND f.tx_mfwcvolunteer_workdepartment = d.uid AND f.tx_mfwcvolunteer_workjobs = j.uid;


UPDATE tx_mfwcvolunteer_domain_model_jobevaluation f, tx_mfwcvolunteer_domain_model_department d, tx_mfwcvolunteer_domain_model_job j
SET f.jobkeep = concat(d.title,' - ',j.title)
WHERE f.department = d.uid AND f.job = j.uid;


############################################### 
####              数据字典              #######
############################################### 

### 志愿者用户表
fe_users(tx_mfwcvolunteer_domain_model_volunteeruser)
志愿者编号                  tx_mfwcvolunteer_id                     varchar(255)
状态                        tx_mfwcvolunteer_status                 int(11)
是否有志愿者经验            tx_mfwcvolunteer_experience             tinyint(1)
技能专长                    tx_mfwcvolunteer_skills                 text
工作职责说明                tx_mfwcvolunteer_responsibilities       text
希望加入的部门              tx_mfwcvolunteer_department             int(11)         ---- 部门数据表
希望加入的志愿者工作岗位    tx_mfwcvolunteer_jobs                   int(11)         ---- 岗位数据表
加入岗位(记录)				tx_mfwcvolunteer_jobskeep				varchar(255)
工作部门                    tx_mfwcvolunteer_workdepartment         int(11)         ---- 部门数据表
工作岗位                    tx_mfwcvolunteer_workjobs               int(11)         ---- 岗位数据表
工作岗位(记录)				tx_mfwcvolunteer_workjobskeep			varchar(255)
工作评价                    tx_mfwcvolunteer_evaluation             int(11)
工作总时长                  tx_mfwcvolunteer_evaluation_length      double(11,2)
认证时间                    tx_mfwcvolunteer_authentication_time    int(11)

### 部门数据表
tx_mfwcvolunteer_domain_model_department
部门名称                    title                   varchar(255)
管理员                      manager                 int(11)
工作岗位                    job                     int(11)    ---- 岗位数据表


### 岗位数据表
tx_mfwcvolunteer_domain_model_job
岗位名称                    title                   varchar(255)
岗位级别                    level                   int(11)
岗位类型                    type                    int(11)
岗位描述                    description             text
岗位目标                    goal                    text
任职资格                    qualifications          text
上级主管                    competent               int(11)       ---- 岗位数据表
归属部门                    department              int(11)       ---- 部门数据表


### 上级主管数据表
tx_mfwcvolunteer_domain_model_competent
上级主管名称  title           varchar(255)


### 工作评价数据表
tx_mfwcvolunteer_domain_model_jobevaluation
工作部门            department         			int(11)         ---- 部门数据表
工作岗位            job               			int(11)         ---- 岗位数据表
工作评价            evluation                   int(11)
工作内容            description                 text
开始时间            fromtime                    int(11)
结束时间            totime                      int(11)
时长                length                      double(11,2)
记录人              recorder                    int(11)


### 部门管理员数据表
tx_mfwcvolunteer_domain_model_departmentmanager
是否发送邮件          sendemail            tinyint(1)
管理员用户            manager_user         int(11)

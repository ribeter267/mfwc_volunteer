(function($){
    $(function(){
    
        $("#jobEvaluation_validation").validate({
			errorElement: "span", 
			rules: {
				'tx_mfwcvolunteer_volunteer[jobEvaluation][evluation]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[jobEvaluation][description]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[jobEvaluation][fromtime]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[jobEvaluation][totime]': {
					required: true,
				}
			},
			messages: {
				'tx_mfwcvolunteer_volunteer[jobEvaluation][evluation]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[jobEvaluation][description]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[jobEvaluation][fromtime]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[jobEvaluation][totime]': {
					required: ""
				}
			}   
        });
		   
		$("#department_validate").validate({
			errorElement: "span", 
			rules: {
				'tx_mfwcvolunteer_volunteer[department][title]': {
					required: true,
				}
			},
			messages: {
				'tx_mfwcvolunteer_volunteer[department][title]': {
					required: ""
				}
			}  
        });
		
		$("#job_validate").validate({
			errorElement: "span", 
			rules: {
				'tx_mfwcvolunteer_volunteer[job][title]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[job][level]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[job][type]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[job][description]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[job][goal]': {
					required: true,
				},
				'tx_mfwcvolunteer_volunteer[job][qualifications]': {
					required: true,
				}
			},
			messages: {
				'tx_mfwcvolunteer_volunteer[job][title]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[job][level]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[job][type]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[job][description]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[job][goal]': {
					required: ""
				},
				'tx_mfwcvolunteer_volunteer[job][qualifications]': {
					required: ""
				}
			}  
        });
    });
})(jQuery);